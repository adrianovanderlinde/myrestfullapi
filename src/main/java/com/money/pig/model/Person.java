package com.money.pig.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="PERSON")
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true, length=11)
	private long id;
	
	@Column(name="NAME", length=255, nullable=false)
	private String name;
	
	@Column(name="SURNAME", length=255, nullable=false)
	private String surname;
	
	@Column(name="BIRTHDAY", nullable=true)
	private Date birthday;
	
	@Column(name="NACIONALITY", length=255, nullable=false)
	private String nationality;
	
	@Column(name="COUNTRY", length=255, nullable=false)
	private String country;
	
	@Column(name="ADDRESS", length=255, nullable=false)
	private String address;
	
	@Column(name="ZIP_NO", length=255, nullable=false)
	private String zipNo;
	
	@Column(name="DOCUMENT_NO", length=255, nullable=false)
	private String documentNo;
	
	@Column(name="DOCUMENT_TYPE", length=255, nullable=false)
	private String documentType;
	
	@OneToOne(mappedBy = "person", cascade = CascadeType.ALL)
	private Account account;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipNo() {
		return zipNo;
	}

	public void setZipNo(String zipNo) {
		this.zipNo = zipNo;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	
	
}
