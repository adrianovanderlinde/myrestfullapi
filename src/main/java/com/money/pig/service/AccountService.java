package com.money.pig.service;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.*;

import java.util.UUID;
import com.money.pig.model.*;
import com.money.pig.dao.PersonDAO;
import com.money.pig.raw.*;

@Path("/account")
public class AccountService {
	
	@Path("/create")
	@RolesAllowed("ADMIN")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String incomingData) {
        ObjectMapper mapper = new ObjectMapper();
		try {
			PersonRaw personRaw = mapper.readValue(incomingData, PersonRaw.class);
			AccountRaw accountRaw = personRaw.getAccount();
			
			Person person = new Person();
			Account account = new Account();
			
			// Person details
			person.setName(personRaw.getName());
			person.setSurname(personRaw.getSurname());
			person.setBirthday(personRaw.getBirthday());
			person.setAddress(personRaw.getAddress());
			person.setZipNo(personRaw.getZipNo());
			person.setCountry(personRaw.getCountry());
			person.setNationality(personRaw.getNationality());
			person.setDocumentNo(personRaw.getDocumentNo());
			person.setDocumentType(personRaw.getDocumentType());
			person.setAccount(account);
			
			// Account details
			UUID accountUUID = UUID.randomUUID();
			
			account.setAccountId(accountUUID.toString());
			account.setEmail(accountRaw.getEmail());
			account.setPhoneNo(accountRaw.getPhoneNo());
			account.setPerson(person);
			// one to one relation
			
            new PersonDAO().save(person);
			
		} catch (Exception e) {
			System.out.println("Error Parsing: - " + e.getMessage());
		}
		System.out.println("Data Received: ");
 
		// return HTTP response 200 in case of success
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson("User created")).build();
	}
	
	@RolesAllowed("ADMIN")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/check")
	public Response getBalance()
	{
		Gson gson = new Gson();
		Person p = new Person();
		p.setName("John");
		p.setSurname("Doe");
		p.setAddress("Test Str. 123");		
		p.setCountry("DE");
		p.setDocumentNo("12345");
		Account ac = new Account();
		ac.setAccountId("ACT000111TEST");
		ac.setEmail("john@doecorp.com");
		ac.setPhoneNo("493333333");
		ac.setPerson(p);
		String json = gson.toJson(ac);
		
		String output = "<h1>Hello check!<h1>" +
				"<p>RESTful Service is running ... <br>Ping @ " + new Date().toString() + "</p<br>";
		output = json;
		return Response.status(200).entity(output).build();
	}

}
