package com.money.pig.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.money.pig.model.Person;
import com.money.pig.util.HibernateUtil;

public class PersonDAO {
	
	private SessionFactory sessionFactory;

	public PersonDAO() {
		sessionFactory = HibernateUtil.getSessionAnnotationFactory();
	}
	
	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Person findById(long id) {
		
		return new Person();	
	}
	
	public Person save(Person person) {
		Session session = this.getCurrentSession();
		
		session.beginTransaction();
		session.saveOrUpdate(person);
		session.getTransaction().commit();
		session.close();
		
		return new Person();
	}
	
	public Person findByName(String name) {
		return new Person();
	}
}
