package com.money.pig.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.money.pig.model.Account;
import com.money.pig.util.HibernateUtil;

public class AccountDAO {
	
	private SessionFactory sessionFactory;

	public AccountDAO() {
		sessionFactory = HibernateUtil.getSessionAnnotationFactory();
	}
	
	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Account findById(long id) {
		
		return new Account();	
	}
	
	public Account save(Account account) {
		Session session = this.getCurrentSession();
		
		session.beginTransaction();		
		session.saveOrUpdate(account);
		session.getTransaction().commit();
		session.close();
		
		return new Account();
	}
	
	public Account findByName(String name) {
		return new Account();
	}

}
