package com.money.pig.controller;

import java.util.Date;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.money.pig.util.HibernateUtil;
import com.money.pig.model.*;

public class SavePerson {
	public SavePerson(Person person) {
		this.store(person);
	}
	
	public SavePerson(String name, 
			String surname,
			Date birthday,
			String nationality,
			String country,
			String address,
			String zipNo,
			String documentNo,
			String documentType) {
		
		Person person = new Person();
		person.setName(name);
		person.setSurname(surname);
		person.setBirthday(birthday);
		person.setNationality(nationality);
		person.setCountry(country);
		person.setAddress(address);
		person.setZipNo(zipNo);
		person.setDocumentNo(documentNo);
		person.setDocumentType(documentType);
		
		this.store(person);
		
	}
	
	private void store(Person person) {
		//Get Session
		SessionFactory sessionFactory = HibernateUtil.getSessionAnnotationFactory();
		Session session = sessionFactory.getCurrentSession();
	
		//start transaction
		session.beginTransaction();
		//Save the Model object
		session.save(person);
		//Commit transaction
		session.getTransaction().commit();
		System.out.println("Person ID="+person.getId());
		
		//terminate session factory, otherwise program won't end
		session.close();
	}
}
