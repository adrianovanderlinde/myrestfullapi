package com.money.pig;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.money.pig.security.AuthenticationFilter;
import com.money.pig.security.CORSFilter;
import javax.ws.rs.ApplicationPath;


@ApplicationPath("/service")
public class MoneyPigApplication extends ResourceConfig {
	
	public MoneyPigApplication()
	{
		packages("com.money.pig");
		register(LoggingFeature.class);
		register(AuthenticationFilter.class);
		register(CORSFilter.class);
		
	}

}
